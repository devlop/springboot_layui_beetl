# springboot_layui_beetl

#### 介绍
springboot框架
layui  前端框架
beetl  dao层框架

#### 软件架构
软件架构说明
后台使用
    springboot 框架 shiro 权限管理
    dao层使用 beetl 新型sql 管理 方便快捷 
前端 
    使用前后端分类的 token 验证方式
    layui 前端框架   
    pandyle.min.js 轻量级模板控制页面按钮权限显示  

页面
![输入图片说明](https://images.gitee.com/uploads/images/2019/0731/105957_04afb2dc_1980247.png "登录.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0731/110010_ad865b90_1980247.png "首页.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0731/110018_7f8c4cab_1980247.png "1564541924(1).png")
#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)