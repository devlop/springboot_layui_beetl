sample
===
* 注释

	select #use("cols")# from sys_users_roles  where  #use("condition")#

cols
===
	id,user_id,createdate,role_id,modifydate,createuser,enterpriseid,deleteflag

updateSample
===
	
	id=#id#,user_id=#userId#,createdate=#createdate#,role_id=#roleId#,modifydate=#modifydate#,createuser=#createuser#,enterpriseid=#enterpriseid#,deleteflag=#deleteflag#

condition
===

	1 = 1  
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(userId)){
	 and user_id=#userId#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(roleId)){
	 and role_id=#roleId#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	@if(!isEmpty(createuser)){
	 and createuser=#createuser#
	@}
	@if(!isEmpty(enterpriseid)){
	 and enterpriseid=#enterpriseid#
	@}
	@if(!isEmpty(deleteflag)){
	 and deleteflag=#deleteflag#
	@}
	
	