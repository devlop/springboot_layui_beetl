searchList
===
* 注释

	select
	 @pageTag(){
	 #use("cols")# 
	 @}
	 from sys_role  where  #use("condition")#
	order by createdate desc

cols
===
	id,enterpriseid,remark,rolename,operateflag,createdate,modifydate,deleteflag,createuser,isoriginaldata,value

updateSample
===
	
	id=#id#,enterpriseid=#enterpriseid#,remark=#remark#,rolename=#rolename#,operateflag=#operateflag#,createdate=#createdate#,modifydate=#modifydate#,deleteflag=#deleteflag#,createuser=#createuser#,isoriginaldata=#isoriginaldata#,value=#value#

condition
===

	1 = 1    and deleteflag=0
    	
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(enterpriseid)){
	 and enterpriseid=#enterpriseid#
	@}
	@if(!isEmpty(remark)){
	 and remark=#remark#
	@}
	@if(!isEmpty(rolename)){
	 and rolename=#rolename#
	@}
	@if(!isEmpty(operateflag)){
	 and operateflag=#operateflag#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	
	@if(!isEmpty(createuser)){
	 and createuser=#createuser#
	@}
	@if(!isEmpty(isoriginaldata)){
	 and isoriginaldata=#isoriginaldata#
	@}
	@if(!isEmpty(value)){
	 and value=#value#
	@}
	
	