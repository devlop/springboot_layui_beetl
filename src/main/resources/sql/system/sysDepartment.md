sample
===
* 注释

	select #use("cols")# from sys_department  where  #use("condition")#

cols
===
	id,name,remark,pid,createdate,modifydate,deleteflag,createuser,enterpriseid

updateSample
===
	
	id=#id#,name=#name#,remark=#remark#,pid=#pid#,createdate=#createdate#,modifydate=#modifydate#,deleteflag=#deleteflag#,createuser=#createuser#,enterpriseid=#enterpriseid#

condition
===

	1 = 1  
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(name)){
	 and name=#name#
	@}
	@if(!isEmpty(remark)){
	 and remark=#remark#
	@}
	@if(!isEmpty(pid)){
	 and pid=#pid#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	@if(!isEmpty(deleteflag)){
	 and deleteflag=#deleteflag#
	@}
	@if(!isEmpty(createuser)){
	 and createuser=#createuser#
	@}
	@if(!isEmpty(enterpriseid)){
	 and enterpriseid=#enterpriseid#
	@}
	
	