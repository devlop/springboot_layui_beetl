sample
===
* 注释

	select #use("cols")# from sys_roles_permissions  where  #use("condition")#

cols
===
	id,role_id,permission_id,createdate,createuser,modifydate,enterpriseid

updateSample
===
	
	id=#id#,role_id=#roleId#,permission_id=#permissionId#,createdate=#createdate#,createuser=#createuser#,modifydate=#modifydate#,enterpriseid=#enterpriseid#

condition
===

	1 = 1  
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(roleId)){
	 and role_id=#roleId#
	@}
	@if(!isEmpty(permissionId)){
	 and permission_id=#permissionId#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(createuser)){
	 and createuser=#createuser#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	@if(!isEmpty(enterpriseid)){
	 and enterpriseid=#enterpriseid#
	@}
	
	