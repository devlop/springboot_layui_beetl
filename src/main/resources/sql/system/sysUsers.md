searchList
===
* 注释

	select 
	@pageTag(){
	#use("cols")# 
	@}
	from sys_users  
	  where  #use("condition")#
		order by createdate desc

cols
===
	id,login_name,login_password,salt,deleteflag,createdate,modifydate,type,enterprise_id,phone,token,department_id,operateflag,picture,login_account,user_grade,logindate,status,sex,email,remark

updateSample
===
	
	id=#id#,login_name=#loginName#,login_password=#loginPassword#,salt=#salt#,deleteflag=#deleteflag#,createdate=#createdate#,modifydate=#modifydate#,type=#type#,enterprise_id=#enterpriseId#,phone=#phone#,token=#token#,department_id=#departmentId#,operateflag=#operateflag#,picture=#picture#,login_account=#loginAccount#,user_grade=#userGrade#,logindate=#logindate#,status=#status#,sex=#sex#,email=#email#,remark=#remark#

condition
===

	1 = 1  
	and deleteflag=0
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(loginName)){
	 and login_name=#loginName#
	@}
	@if(!isEmpty(loginPassword)){
	 and login_password=#loginPassword#
	@}
	@if(!isEmpty(salt)){
	 and salt=#salt#
	@}
	@if(!isEmpty(deleteflag)){
	 and deleteflag=#deleteflag#
	@}
	@if(!isEmpty(createdate)){
	 and createdate=#createdate#
	@}
	@if(!isEmpty(modifydate)){
	 and modifydate=#modifydate#
	@}
	@if(!isEmpty(type)){
	 and type=#type#
	@}
	@if(!isEmpty(enterpriseId)){
	 and enterprise_id=#enterpriseId#
	@}
	@if(!isEmpty(phone)){
	 and phone=#phone#
	@}
	@if(!isEmpty(departmentId)){
	 and department_id=#departmentId#
	@}
	@if(!isEmpty(operateflag)){
	 and operateflag=#operateflag#
	@}
	@if(!isEmpty(picture)){
	 and picture=#picture#
	@}
	@if(!isEmpty(loginAccount)){
	 and login_account=#loginAccount#
	@}
	@if(!isEmpty(userGrade)){
	 and user_grade=#userGrade#
	@}
	@if(!isEmpty(logindate)){
	 and logindate=#logindate#
	@}
	@if(!isEmpty(status)){
	 and status=#status#
	@}
	@if(!isEmpty(sex)){
	 and sex=#sex#
	@}
	@if(!isEmpty(email)){
	 and email=#email#
	@}
	@if(!isEmpty(remark)){
	 and remark=#remark#
	@}
	
	