layui.config({
    base: '/static/lib/layui/module/'
}).extend({ //设定模块别名
     Ad: 'Ad'
    ,cfg: 'cfg'
});

var tableIns;

layui.use(['form','layer','table','cfg','Ad'],function(){
	  var $ = layui.jquery;
      var table = layui.table;
      var layer = layui.layer;
      var form = layui.form;
      var  cfg = layui.cfg;
      var  Ad = layui.Ad;

        //行工具栏
        $('#tool').vm({
            editShow: true,
            delShow:true
        });

        //头部工具栏
        $('#toolbar').vm({
            addShow:true
        });


    //用户列表
     tableIns = table.render({
        elem: '#tableList',
        url : cfg.getTokenUrl('/system/users/searchList'),
        cellMinWidth : 80,
        page : true,
        height : "full-120",
        limits : [10,15,20,25],
        limit : 20,
        id : "tableList",
        toolbar: '#toolbar',
        where: {

        },
        cols : [[
            {type: "checkbox", fixed:"left", width:50},
            {field: 'loginName', title: '用户名', minWidth:50, align:"center"},
            {field: 'loginAccount', title: '用户帐号', minWidth:80, align:"center"},
            {field: 'sex', title: '用户性别', align:'center',templet:function(d){
                if(d.sex == "0"){
                    return "男";
                }else if(d.sex == "1"){
                    return "女";
                }else if(d.sex == "2"){
                    return "保密";
                }
            }},
            {field: 'status', title: '用户状态',  align:'center',templet:function(d){
                return d.status == "0" ? "正常使用" : "限制使用";
            }},
            {field: 'userGrade', title: '用户等级', align:'center',templet:function(d){
                if(d.userGrade == "0"){
                    return "注册会员";
                }else if(d.userGrade == "1"){
                    return "中级会员";
                }else if(d.userGrade == "2"){
                    return "高级会员";
                }else if(d.userGrade == "3"){
                    return "钻石会员";
                }else if(d.userGrade == "4"){
                    return "超级会员";
                }
            }},
            {field: 'createdate', title: '创建时间', align:'center',minWidth:150},
            {field: 'logindate', title: '最后登录时间', align:'center',minWidth:150},
            {title: '操作', minWidth:175, templet:'#tool',fixed:"right",align:"center"}
        ]]
    });
    
    //监听表单搜索
    form.on('submit(searchBtn)', function(data){
    	var formdata=data.field;
    	table.reload("tableList",{
            page: {
                curr: 1 //重新从第 1 页开始
            },
            where: formdata
        });
      return false;
    });

    //监听头部工具栏
    table.on('toolbar(tableList)', function(obj){
        var checkStatus = table.checkStatus(obj.config.id);
        switch(obj.event){
            case 'add':
                $("#selectId").val("");
                Ad.popLayer('/templates/system/users/userInput.html','添加用户');
                break;
        }
    });

    //监听行工具条
    table.on('tool(tableList)', function(obj){
        var layEvent = obj.event,
            data = obj.data;

        if(layEvent === 'edit'){ //编辑
            $("#selectId").val(data.id);
            Ad.popLayer('/templates/system/users/userInput.html','编辑用户');
        }else if(layEvent === 'usable'){ //启用禁用


        }else if(layEvent === 'del'){ //删除
            layer.confirm('确定删除此用户？',{icon:3, title:'提示信息'},function(index){
                Ad.post("/system/users/delete",{
                    id : data.id  //将需要删除的newsId作为参数传入
                },function(data){
                    Ad.Msg(data);
                    tableIns.reload();
                })
            });
        }
    });
});