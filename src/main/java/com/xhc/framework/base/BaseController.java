package com.xhc.framework.base;

import com.xhc.common.utils.ServletUtils;
import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.system.entity.SysUsers;
import org.apache.poi.ss.formula.functions.T;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.engine.PageQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(BaseController.class);

    protected final String limit = "limit"; //一页多少数据
    protected final String page = "page";  //查询第几页数据


    @Autowired
    SQLManager sqlManager;

    /**
     * 获取requests
     */
    public HttpServletRequest getRequest() {
        return ServletUtils.getRequest();
    }

    /**
     * 获取response
     */
    public HttpServletResponse getResponse() {
        return ServletUtils.getResponse();
    }

    /**
     * 获取session
     */
    public HttpSession getSession() {
        return getRequest().getSession();
    }

    /**
     * 返回成功
     */
    public AjaxResult success() {
        return new AjaxResult().success();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String msg) {
        return new AjaxResult().success(msg);
    }


    /**
     * 返回警告的信息
     */
    public AjaxResult warn() {
        return new AjaxResult().warn();
    }

    /**
     * 返回警告的信息
     */
    public AjaxResult warn(String msg) {
        return new AjaxResult().warn(msg);
    }


    /**
     * 根据beetl 查询结果返回layui table 的数据格式
     *
     * @param pageQuery
     * @returnd
     */
    protected GridDataTable getGridDataTable(PageQuery<T> pageQuery) {
        return new GridDataTable(pageQuery.getList(), pageQuery.getTotalRow());
    }

    /**
     * 得到分页查询的对象封装参数
     *
     * @return
     */
    protected PageQuery getPageQuery() {

        String strpage = getRequest().getParameter(page);
        String strlimit = getRequest().getParameter(limit);
        PageQuery pageQuery = new PageQuery();
        if (isNotEmpty(strpage)) {
            pageQuery.setPageNumber(Long.parseLong(strpage));
        }
        if (isNotEmpty(strlimit)) {
            pageQuery.setPageSize(Long.parseLong(strlimit));
        }
        Map<String, String[]> parameterMap = getRequest().getParameterMap();
        Map<String, Object> map = new HashMap<String, Object>();
        for (String key : parameterMap.keySet()) {
            String value = parameterMap.get(key)[0];
            if (isNotEmpty(value)) {
                map.put(key, value);
            }
        }
        pageQuery.setParas(map);
        return pageQuery;
    }


    /**
     * 分页查询 这里 默认数据源 多数据源的话 需要手动修改
     *
     * @param sqlId
     * @return
     */
    protected GridDataTable getPageTableData(String sqlId, Class clazz) {
        PageQuery pageQuery = sqlManager.pageQuery(sqlId, clazz, getPageQuery());
        GridDataTable gridDataTable = getGridDataTable(pageQuery);
        return gridDataTable;
    }


    /**
     * 是否不为空
     *
     * @author changwei
     * @date 2018年12月19日
     */
    protected boolean isNotEmpty(Object obj) {
        return !isEmpty(obj);
    }

    /**
     * 是否为空
     *
     * @author changwei
     * @date 2018年12月19日
     */
    protected boolean isEmpty(Object obj) {
        if (obj == null)
            return true;
        if (obj instanceof String) {
            String cs = (String) obj;
            int strLen;
            if (cs == null || (strLen = cs.length()) == 0) {
                return true;
            }
            for (int i = 0; i < strLen; i++) {
                if (Character.isWhitespace(cs.charAt(i)) == false) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map)
            return ((Map<?, ?>) obj).isEmpty();
        if (obj instanceof Collection)
            return ((Collection<?>) obj).isEmpty();
        if (obj instanceof Set)
            return ((Set<?>) obj).isEmpty();
        if (obj instanceof Object[])
            return ((Object[]) obj).length == 0;
        return false;
    }
    /**
     * 保存或者修改接口
     *
     * @author changwei
     * @date 2019年1月14日 上午9:39:03
     */
  //  public abstract AjaxResult saveOrUpdate(Object obj);


    public abstract AjaxResult get(String id);

    /**
     * 删除接口
     *
     * @author changwei
     * @date 2019年1月14日 上午9:38:51
     */
    public abstract AjaxResult delete(String id);

    /**
     * 分页查询
     *
     * @author changwei
     * @date 2019年1月14日 上午9:38:11
     */
    public abstract GridDataTable searchList();


}
