package com.xhc.framework.config.shiro.session;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;

import java.io.Serializable;

/**
 * 针对自定义的ShiroSession的db操作
 * 
 * @author ruoyi
 */
public class OnlineSessionDAO extends EnterpriseCacheSessionDAO
{
    @Override
    protected Serializable doCreate(Session session) {
        System.out.println("doCreate"+session.toString());
        return super.doCreate(session);
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        System.out.println("doReadSession sessionId"+sessionId);
        return super.doReadSession(sessionId);
    }

    @Override
    protected void doUpdate(Session session) {
        System.out.println("doUpdate"+session.toString());
        super.doUpdate(session);
    }

    @Override
    protected void doDelete(Session session) {
        System.out.println("doDelete"+session.toString());
        super.doDelete(session);
    }
}
