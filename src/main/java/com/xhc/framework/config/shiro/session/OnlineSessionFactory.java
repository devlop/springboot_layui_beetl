package com.xhc.framework.config.shiro.session;

import javax.servlet.http.HttpServletRequest;

import com.xhc.common.utils.IpUtils;
import com.xhc.common.utils.RandomUtil;
import com.xhc.common.utils.ServletUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionFactory;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.web.session.mgt.WebSessionContext;
import org.springframework.stereotype.Component;


/**
 * 自定义sessionFactory会话
 * 
 * @author ruoyi
 */
@Component
public class OnlineSessionFactory implements SessionFactory
{
    @Override
    public Session createSession(SessionContext initData)
    {



        if (initData != null) {
            String host = initData.getHost();
            if (host != null) {
                OnlineSession  session = new OnlineSession();
                session.setHost(host);
                System.out.println("创建sessionid"+session.toString());
                return session;
            }
        }
        System.out.println("创建sessionid initData=null");
        return new OnlineSession();

    }
}
