package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.system.entity.SysUsers;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
public class SysLoginController {
    @PostMapping("/login")
    public AjaxResult login(String username, String password, Boolean rememberMe) {
        System.out.println(username);
        System.out.println(password);
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        try {
            if(!subject.isAuthenticated()){
                subject.login(token);
            }
            SysUsers user = (SysUsers)subject.getPrincipal();
            user.setToken(subject.getSession().getId().toString());
            String msg = "登录成功";
            return new AjaxResult().success(msg,user);
        } catch (AuthenticationException e) {
            String msg = "用户或密码错误";
            if (e.getMessage() != null) {
                msg = e.getMessage();
            }
            return new AjaxResult().warn(msg);
        }

    }

    @PostMapping("/loginOut")
    public AjaxResult loginOut() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        String msg = "退出成功";
        return new AjaxResult().success(msg);
    }


}
