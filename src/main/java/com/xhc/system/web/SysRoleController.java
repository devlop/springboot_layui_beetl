package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.framework.base.BaseController;
import com.xhc.system.entity.SysRole;
import com.xhc.system.entity.SysUsers;
import com.xhc.system.services.SysRoleService;
import com.xhc.system.services.SysUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController {


    @Autowired
    SysRoleService sysRoleService;

    @PostMapping("/saveOrUpdate")
    public AjaxResult saveOrUpdate(SysRole sysRole) {
        return sysRoleService.saveOrUpdate(sysRole);
    }

    @GetMapping("/get")
    public AjaxResult get(String id) {
        return sysRoleService.get(id);
    }

    @PostMapping("/delete")
    public AjaxResult delete(String id) {
        return sysRoleService.delete(id);
    }

    @GetMapping("/searchList")
    public GridDataTable searchList() {
        GridDataTable pageTableData = getPageTableData("system.sysRole.searchList", SysRole.class);
        return pageTableData;
    }
}
