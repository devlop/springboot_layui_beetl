package com.xhc.system.web;

import com.xhc.common.vo.AjaxResult;
import com.xhc.common.vo.GridDataTable;
import com.xhc.framework.base.BaseController;
import com.xhc.system.dao.SysUsersDao;
import com.xhc.system.entity.SysUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/system/test")
public class TestController extends BaseController {

    @Autowired
    SysUsersDao sysUserDao;


    @GetMapping("/test")
    public List<SysUsers> index() {

        List<SysUsers> sysUserList = sysUserDao.all();
        return sysUserList;
    }


    @Override
    public AjaxResult get(String id) {
        return null;
    }

    @Override
    public AjaxResult delete(String id) {
        return null;
    }


    @GetMapping("/searchList")
    public GridDataTable searchList() {

        GridDataTable pageTableData = getPageTableData("system.sysUsers.sample",SysUsers.class);
        return pageTableData;
    }
}