package com.xhc.system.vo;

import java.util.List;

/**
 * 页面菜单
 * 
 * @author changwei
 * @date 2018年12月14日
 */
public class Menu {
	
	private String id;
	
	private String pid;
	
	/**
	 * 标题
	 */
	private String title;
	/**
	 * 图片
	 */
	private String icon;
	/**
	 * 跳转的URl
	 */
	private String href;
	/**
	 * 
	 */
	private boolean spread = false;
	/**
	 * 打开新的页面 _blank
	 */
	private String target;
	/**
	 * 子集集合
	 */
	private List<Menu> children;

	public Menu(String title, String icon, String href, boolean spread, String target) {
		this.title = title;
		this.icon = icon;
		this.href = href;
		this.spread = spread;
		this.target = target;
	}

	public List<Menu> getChildren() {
		return children;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setChildren(List<Menu> children) {
		this.children = children;
	}

	public Menu() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public boolean isSpread() {
		return spread;
	}

	public void setSpread(boolean spread) {
		this.spread = spread;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}
