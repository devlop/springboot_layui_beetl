package com.xhc.system.dao;

import com.xhc.system.entity.SysExceptionRecord;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysExceptionRecord")
public interface SysExceptionRecordDao extends BaseMapper<SysExceptionRecord> {
}