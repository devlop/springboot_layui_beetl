package com.xhc.system.dao;

import com.xhc.system.entity.SysRolesPermissions;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysRolesPermissions")
public interface SysRolesPermissionsDao extends BaseMapper<SysRolesPermissions> {
}
