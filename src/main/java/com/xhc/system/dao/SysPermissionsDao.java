package com.xhc.system.dao;

import com.xhc.system.entity.SysPermissions;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@SqlResource("system.sysPermissions")
public interface SysPermissionsDao extends BaseMapper<SysPermissions> {


    public List<SysPermissions> getPermissionsList();



}
