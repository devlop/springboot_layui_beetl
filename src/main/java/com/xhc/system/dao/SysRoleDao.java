package com.xhc.system.dao;

import com.xhc.system.entity.SysRole;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysRole")
public interface SysRoleDao extends BaseMapper<SysRole> {
}
