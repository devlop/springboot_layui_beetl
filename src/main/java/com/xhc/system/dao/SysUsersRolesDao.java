package com.xhc.system.dao;

import com.xhc.system.entity.SysUsersRoles;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

@Component
@SqlResource("system.sysUsersRoles")
public interface SysUsersRolesDao extends BaseMapper<SysUsersRoles> {
}
