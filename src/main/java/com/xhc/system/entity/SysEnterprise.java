package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_enterprise")
public class SysEnterprise   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_telephone = "telephone";
	public static final String ALIAS_address = "address";
	public static final String ALIAS_code = "code";
	public static final String ALIAS_mobile = "mobile";
	public static final String ALIAS_name = "name";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	@AssignID()
	private String id ;
	/*
	手机号
	*/
	private Integer telephone ;
	/*
	企业地址
	*/
	private String address ;
	/*
	企业id编号唯一 登录的时候要用到
	*/
	private String code ;
	/*
	电话
	*/
	private String mobile ;
	/*
	企业名称
	*/
	private String name ;
	private Date createdate ;
	private Date modifydate ;
	
	public SysEnterprise() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	/**
	* 手机号
	*@return 
	*/
	public Integer getTelephone(){
		return  telephone;
	}
	/**
	* 手机号
	*@param  telephone
	*/
	public void setTelephone(Integer telephone ){
		this.telephone = telephone;
	}
	
	/**
	* 企业地址
	*@return 
	*/
	public String getAddress(){
		return  address;
	}
	/**
	* 企业地址
	*@param  address
	*/
	public void setAddress(String address ){
		this.address = address;
	}
	
	/**
	* 企业id编号唯一 登录的时候要用到
	*@return 
	*/
	public String getCode(){
		return  code;
	}
	/**
	* 企业id编号唯一 登录的时候要用到
	*@param  code
	*/
	public void setCode(String code ){
		this.code = code;
	}
	
	/**
	* 电话
	*@return 
	*/
	public String getMobile(){
		return  mobile;
	}
	/**
	* 电话
	*@param  mobile
	*/
	public void setMobile(String mobile ){
		this.mobile = mobile;
	}
	
	/**
	* 企业名称
	*@return 
	*/
	public String getName(){
		return  name;
	}
	/**
	* 企业名称
	*@param  name
	*/
	public void setName(String name ){
		this.name = name;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
