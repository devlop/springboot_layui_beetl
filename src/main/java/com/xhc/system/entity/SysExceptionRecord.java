package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_exception_record")
public class SysExceptionRecord   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_message = "message";
	public static final String ALIAS_url = "url";
	public static final String ALIAS_createdate = "createdate";
	@AssignID()
	private String id ;
	private String message ;
	private String url ;
	private Date createdate ;
	
	public SysExceptionRecord() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public String getMessage(){
		return  message;
	}
	public void setMessage(String message ){
		this.message = message;
	}
	
	public String getUrl(){
		return  url;
	}
	public void setUrl(String url ){
		this.url = url;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	

}
