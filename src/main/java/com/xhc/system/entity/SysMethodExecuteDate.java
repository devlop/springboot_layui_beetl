package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_method_execute_date")
public class SysMethodExecuteDate   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_methoddate = "methoddate";
	public static final String ALIAS_url = "url";
	public static final String ALIAS_createdate = "createdate";
	@AssignID()
	private String id ;
	/*
	执行时间
	*/
	private Integer methoddate ;
	/*
	接口名称
	*/
	private String url ;
	private Date createdate ;
	
	public SysMethodExecuteDate() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	/**
	* 执行时间
	*@return 
	*/
	public Integer getMethoddate(){
		return  methoddate;
	}
	/**
	* 执行时间
	*@param  methoddate
	*/
	public void setMethoddate(Integer methoddate ){
		this.methoddate = methoddate;
	}
	
	/**
	* 接口名称
	*@return 
	*/
	public String getUrl(){
		return  url;
	}
	/**
	* 接口名称
	*@param  url
	*/
	public void setUrl(String url ){
		this.url = url;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	

}
