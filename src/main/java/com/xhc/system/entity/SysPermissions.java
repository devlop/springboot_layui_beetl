package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_permissions")
public class SysPermissions   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_deleteflag = "deleteflag";
	public static final String ALIAS_operateflag = "operateflag";
	public static final String ALIAS_spread = "spread";
	public static final String ALIAS_type = "type";
	public static final String ALIAS_href = "href";
	public static final String ALIAS_icon = "icon";
	public static final String ALIAS_pid = "pid";
	public static final String ALIAS_target = "target";
	public static final String ALIAS_title = "title";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	public static final String ALIAS_num = "num";
	@AssignID()
	private String id ;
	/*
	是否删除
	*/
	private Integer deleteflag ;
	/*
	0启用 是否启用 -1不启用
	*/
	private Integer operateflag ;
	private Integer spread ;
	/*
	0 菜单 1按钮
	*/
	private Integer type ;
	/*
	角色的值 如果为按钮 和 菜单 结构不一样
	*/
	private String href ;
	private String icon ;
	private String pid ;
	private String target ;
	private String title ;
	private Date createdate ;
	private Date modifydate ;
	/*
	排序
	*/
	private Integer num ;
	
	public SysPermissions() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	/**
	* 是否删除
	*@return 
	*/
	public Integer getDeleteflag(){
		return  deleteflag;
	}
	/**
	* 是否删除
	*@param  deleteflag
	*/
	public void setDeleteflag(Integer deleteflag ){
		this.deleteflag = deleteflag;
	}
	
	/**
	* 0启用 是否启用 -1不启用
	*@return 
	*/
	public Integer getOperateflag(){
		return  operateflag;
	}
	/**
	* 0启用 是否启用 -1不启用
	*@param  operateflag
	*/
	public void setOperateflag(Integer operateflag ){
		this.operateflag = operateflag;
	}
	
	public Integer getSpread(){
		return  spread;
	}
	public void setSpread(Integer spread ){
		this.spread = spread;
	}
	
	/**
	* 0 菜单 1按钮
	*@return 
	*/
	public Integer getType(){
		return  type;
	}
	/**
	* 0 菜单 1按钮
	*@param  type
	*/
	public void setType(Integer type ){
		this.type = type;
	}
	
	/**
	* 角色的值 如果为按钮 和 菜单 结构不一样
	*@return 
	*/
	public String getHref(){
		return  href;
	}
	/**
	* 角色的值 如果为按钮 和 菜单 结构不一样
	*@param  href
	*/
	public void setHref(String href ){
		this.href = href;
	}
	
	public String getIcon(){
		return  icon;
	}
	public void setIcon(String icon ){
		this.icon = icon;
	}
	
	public String getPid(){
		return  pid;
	}
	public void setPid(String pid ){
		this.pid = pid;
	}
	
	public String getTarget(){
		return  target;
	}
	public void setTarget(String target ){
		this.target = target;
	}
	
	public String getTitle(){
		return  title;
	}
	public void setTitle(String title ){
		this.title = title;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	
	/**
	* 排序
	*@return 
	*/
	public Integer getNum(){
		return  num;
	}
	/**
	* 排序
	*@param  num
	*/
	public void setNum(Integer num ){
		this.num = num;
	}
	

}
