package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;
import org.springframework.format.annotation.DateTimeFormat;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_users")
public class SysUsers   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_deleteflag = "deleteflag";
	public static final String ALIAS_operateflag = "operateflag";
	public static final String ALIAS_sex = "sex";
	public static final String ALIAS_status = "status";
	public static final String ALIAS_type = "type";
	public static final String ALIAS_user_grade = "user_grade";
	public static final String ALIAS_department_id = "department_id";
	public static final String ALIAS_email = "email";
	public static final String ALIAS_enterprise_id = "enterprise_id";
	public static final String ALIAS_login_account = "login_account";
	public static final String ALIAS_login_name = "login_name";
	public static final String ALIAS_login_password = "login_password";
	public static final String ALIAS_phone = "phone";
	public static final String ALIAS_picture = "picture";
	public static final String ALIAS_remark = "remark";
	public static final String ALIAS_salt = "salt";
	public static final String ALIAS_token = "token";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_logindate = "logindate";
	public static final String ALIAS_modifydate = "modifydate";


	@AssignID()
	private String id ;
	private Integer deleteflag ;
	/*
	是否启用 -1不启用
	*/
	private Integer operateflag ;
	/*
	性别
	*/
	private Integer sex ;
	/*
	状态
	*/
	private Integer status ;
	private Integer type ;
	/*
	用户等级
	*/
	private Integer userGrade ;
	private String departmentId ;
	/*
	邮箱
	*/
	private String email ;
	private String enterpriseId ;
	/*
	登录帐号
	*/
	private String loginAccount ;
	/*
	登录的用户名
	*/
	private String loginName ;
	private String loginPassword ;
	/*
	手机号
	*/
	private String phone ;
	/*
	用户的头像
	*/
	private String picture ;
	private String remark ;
	/*
	加密的盐
	*/
	private String salt ;
	/*
	登录的验证
	*/
	private String token ;

	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createdate ;
	/*
	最后登录时间
	*/
	private Date logindate ;
	private Date modifydate ;
	
	public SysUsers() {
	}

	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public Integer getDeleteflag(){
		return  deleteflag;
	}
	public void setDeleteflag(Integer deleteflag ){
		this.deleteflag = deleteflag;
	}
	
	/**
	* 是否启用 -1不启用
	*@return 
	*/
	public Integer getOperateflag(){
		return  operateflag;
	}
	/**
	* 是否启用 -1不启用
	*@param  operateflag
	*/
	public void setOperateflag(Integer operateflag ){
		this.operateflag = operateflag;
	}
	
	/**
	* 性别
	*@return 
	*/
	public Integer getSex(){
		return  sex;
	}
	/**
	* 性别
	*@param  sex
	*/
	public void setSex(Integer sex ){
		this.sex = sex;
	}
	
	/**
	* 状态
	*@return 
	*/
	public Integer getStatus(){
		return  status;
	}
	/**
	* 状态
	*@param  status
	*/
	public void setStatus(Integer status ){
		this.status = status;
	}
	
	public Integer getType(){
		return  type;
	}
	public void setType(Integer type ){
		this.type = type;
	}
	
	/**
	* 用户等级
	*@return 
	*/
	public Integer getUserGrade(){
		return  userGrade;
	}
	/**
	* 用户等级
	*@param  userGrade
	*/
	public void setUserGrade(Integer userGrade ){
		this.userGrade = userGrade;
	}
	
	public String getDepartmentId(){
		return  departmentId;
	}
	public void setDepartmentId(String departmentId ){
		this.departmentId = departmentId;
	}
	
	/**
	* 邮箱
	*@return 
	*/
	public String getEmail(){
		return  email;
	}
	/**
	* 邮箱
	*@param  email
	*/
	public void setEmail(String email ){
		this.email = email;
	}
	
	public String getEnterpriseId(){
		return  enterpriseId;
	}
	public void setEnterpriseId(String enterpriseId ){
		this.enterpriseId = enterpriseId;
	}
	
	/**
	* 登录帐号
	*@return 
	*/
	public String getLoginAccount(){
		return  loginAccount;
	}
	/**
	* 登录帐号
	*@param  loginAccount
	*/
	public void setLoginAccount(String loginAccount ){
		this.loginAccount = loginAccount;
	}
	
	/**
	* 登录的用户名
	*@return 
	*/
	public String getLoginName(){
		return  loginName;
	}
	/**
	* 登录的用户名
	*@param  loginName
	*/
	public void setLoginName(String loginName ){
		this.loginName = loginName;
	}
	
	public String getLoginPassword(){
		return  loginPassword;
	}
	public void setLoginPassword(String loginPassword ){
		this.loginPassword = loginPassword;
	}
	
	/**
	* 手机号
	*@return 
	*/
	public String getPhone(){
		return  phone;
	}
	/**
	* 手机号
	*@param  phone
	*/
	public void setPhone(String phone ){
		this.phone = phone;
	}
	
	/**
	* 用户的头像
	*@return 
	*/
	public String getPicture(){
		return  picture;
	}
	/**
	* 用户的头像
	*@param  picture
	*/
	public void setPicture(String picture ){
		this.picture = picture;
	}
	
	public String getRemark(){
		return  remark;
	}
	public void setRemark(String remark ){
		this.remark = remark;
	}
	
	/**
	* 加密的盐
	*@return 
	*/
	public String getSalt(){
		return  salt;
	}
	/**
	* 加密的盐
	*@param  salt
	*/
	public void setSalt(String salt ){
		this.salt = salt;
	}
	
	/**
	* 登录的验证
	*@return 
	*/
	public String getToken(){
		return  token;
	}
	/**
	* 登录的验证
	*@param  token
	*/
	public void setToken(String token ){
		this.token = token;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	/**
	* 最后登录时间
	*@return 
	*/
	public Date getLogindate(){
		return  logindate;
	}
	/**
	* 最后登录时间
	*@param  logindate
	*/
	public void setLogindate(Date logindate ){
		this.logindate = logindate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
