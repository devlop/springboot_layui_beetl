package com.xhc.system.entity;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2019-07-18
*/
@Table(name="sb.sys_department")
public class SysDepartment   {
	
	// alias
	public static final String ALIAS_id = "id";
	public static final String ALIAS_deleteflag = "deleteflag";
	public static final String ALIAS_createuser = "createuser";
	public static final String ALIAS_enterpriseid = "enterpriseid";
	public static final String ALIAS_name = "name";
	public static final String ALIAS_pid = "pid";
	public static final String ALIAS_remark = "remark";
	public static final String ALIAS_createdate = "createdate";
	public static final String ALIAS_modifydate = "modifydate";
	@AssignID()
	private String id ;
	private Integer deleteflag ;
	private String createuser ;
	private String enterpriseid ;
	/*
	部门名称
	*/
	private String name ;
	/*
	上级部门的ID
	*/
	private String pid ;
	private String remark ;
	private Date createdate ;
	private Date modifydate ;
	
	public SysDepartment() {
	}
	
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public Integer getDeleteflag(){
		return  deleteflag;
	}
	public void setDeleteflag(Integer deleteflag ){
		this.deleteflag = deleteflag;
	}
	
	public String getCreateuser(){
		return  createuser;
	}
	public void setCreateuser(String createuser ){
		this.createuser = createuser;
	}
	
	public String getEnterpriseid(){
		return  enterpriseid;
	}
	public void setEnterpriseid(String enterpriseid ){
		this.enterpriseid = enterpriseid;
	}
	
	/**
	* 部门名称
	*@return 
	*/
	public String getName(){
		return  name;
	}
	/**
	* 部门名称
	*@param  name
	*/
	public void setName(String name ){
		this.name = name;
	}
	
	/**
	* 上级部门的ID
	*@return 
	*/
	public String getPid(){
		return  pid;
	}
	/**
	* 上级部门的ID
	*@param  pid
	*/
	public void setPid(String pid ){
		this.pid = pid;
	}
	
	public String getRemark(){
		return  remark;
	}
	public void setRemark(String remark ){
		this.remark = remark;
	}
	
	public Date getCreatedate(){
		return  createdate;
	}
	public void setCreatedate(Date createdate ){
		this.createdate = createdate;
	}
	
	public Date getModifydate(){
		return  modifydate;
	}
	public void setModifydate(Date modifydate ){
		this.modifydate = modifydate;
	}
	

}
