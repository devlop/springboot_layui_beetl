package com.xhc.system.services;

import com.xhc.common.utils.RandomUtil;
import com.xhc.common.vo.AjaxResult;
import com.xhc.framework.base.BaseService;
import com.xhc.system.dao.SysPermissionsDao;
import com.xhc.system.entity.SysPermissions;
import com.xhc.system.entity.SysUsers;
import com.xhc.system.vo.DTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SysPermissionsService extends BaseService {
    @Autowired
    SysPermissionsDao sysPermissionsDao;


    public AjaxResult get(String id) {
        SysPermissions sysPermissions = sysPermissionsDao.unique(id);
        return success(sysPermissions);
    }

    public AjaxResult delete(String id) {
        SysPermissions sysPermissions = new SysPermissions();
        sysPermissions.setId(id);
        sysPermissions.setDeleteflag(1);
        int i = this.sysPermissionsDao.updateTemplateById(sysPermissions);
        return success();
    }

    public AjaxResult saveOrUpdate(SysPermissions sysPermissions) {
        if (isNotEmpty(sysPermissions.getId())) {
            sysPermissionsDao.updateTemplateById(sysPermissions);
        } else {
            sysPermissions.setId(RandomUtil.getUuidByJdk());
            sysPermissions.setCreatedate(new Date());
            sysPermissionsDao.insertTemplate(sysPermissions);

        }
        return success();
    }

    public AjaxResult getTreeData() {
        SysPermissions per = new SysPermissions();
        List<SysPermissions> permissionsList = sysPermissionsDao.getPermissionsList();
        List<DTree> eleTreeList = new ArrayList<DTree>();
        // 转化下对象
        for (SysPermissions permissions : permissionsList) {
            DTree eleTree = new DTree(permissions.getId(), permissions.getPid(), permissions.getTitle(),
                    permissions.getHref(), permissions.getIcon(), permissions.getType(), "0", permissions.getOperateflag(),
                    null);
            eleTree.setNum(permissions.getNum());
            eleTreeList.add(eleTree);
        }
        // 查询对象中的下级集合
        for (DTree eleTree : eleTreeList) {
            List<DTree> childrenEleTreeList = new ArrayList<DTree>();
            for (DTree eleTree1 : eleTreeList) {
                if (eleTree1.getParentId() != null && eleTree.getId().equals(eleTree1.getParentId())) {
                    childrenEleTreeList.add(eleTree1);
                }
            }
            eleTree.setChildren(childrenEleTreeList);
        }
        // 得到顶层的数据
        List<DTree> PList = new ArrayList<DTree>();
        for (DTree eleTree : eleTreeList) {
            if (eleTree.getParentId() == null || eleTree.getParentId().equals("-1")) {
                PList.add(eleTree);
            }
        }
      return   new AjaxResult().success(PList);
    }

}
