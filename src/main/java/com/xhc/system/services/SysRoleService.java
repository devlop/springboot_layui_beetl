package com.xhc.system.services;

import com.xhc.common.utils.RandomUtil;
import com.xhc.common.vo.AjaxResult;
import com.xhc.framework.base.BaseService;
import com.xhc.system.dao.SysRoleDao;
import com.xhc.system.entity.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SysRoleService extends BaseService {


    @Autowired
    SysRoleDao sysRoleDao;

    public AjaxResult get(String id) {
        SysRole sysRole = sysRoleDao.unique(id);
        return success(sysRole);
    }

    public AjaxResult delete(String id) {
        SysRole sysRole = new SysRole();
        sysRole.setId(id);
        sysRole.setDeleteflag(1);
        int i = this.sysRoleDao.updateTemplateById(sysRole);
        return success();
    }

    public AjaxResult saveOrUpdate(SysRole sysRole) {
        if (isNotEmpty(sysRole.getId())) {
            sysRoleDao.updateTemplateById(sysRole);
        } else {
            sysRole.setId(RandomUtil.getUuidByJdk());
            sysRole.setCreatedate(new Date());
            sysRoleDao.insertTemplate(sysRole);

        }
        return success();
    }


}
