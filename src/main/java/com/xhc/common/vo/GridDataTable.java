/**
 * Copyright: Copyright (c) 2012
 * Company:深圳市海乐淘电子商务有限公司
 * @author liuf
 * @date 2013-2-27 下午4:54:13
 * @version V1.0
 */
package com.xhc.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 返回layui的table数据
 * 
 * @author changwei
 * @date 2018-5-10 下午6:43:10
 * @param <T>
 */
public class GridDataTable<T> implements Serializable {

	private static final long serialVersionUID = 1392958071567626594L;
    /**
     * 返回的数据
     */
	private List<T> data;
	/**
	 * 总行数
	 */
	private long count;

	private String msg = "";
	private int code = 0;

	public GridDataTable(List<T> data, long count) {
		this.data = data;
		this.count = count;
	}

	/**
	 * @return the data
	 */
	public List<T> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<T> data) {
		this.data = data;
	}

	/**
	 * @return the count
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(long count) {
		this.count = count;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
