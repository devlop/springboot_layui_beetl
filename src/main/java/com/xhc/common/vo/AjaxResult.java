package com.xhc.common.vo;


/**
 * 操作消息提醒
 * 
 * @author ruoyi
 */
public class AjaxResult
{

    /**
     * 状态类型
     */
    public enum Type
    {
        /** 成功 */
        SUCCESS(0),
        /** 警告 */
        WARN(1),
        /** 系统异常发生错误 */
        ERROR(2),
        /** 没有权限或者权限不足 或者没有登录*/
        NO_PERMISSION(3),
        /** 没有资源 */
        NO_RESOURCE(4);

        private final int value;

        Type(int value)
        {
            this.value = value;
        }

        public int value()
        {
            return this.value;
        }
    }
    /** 总条数 */
    private int count=0;
    /** 状态码 */
    private int code=0;

    /** 返回内容 */
    private String msg;

    /** 数据对象 */
    private Object data;

    /**
     * 初始化一个新创建的 AjaxResult 对象，使其表示一个空消息。
     */
    public AjaxResult()
    {
    }


    /**
     * 初始化一个新创建的 AjaxResult 对象
     * 
     * @param type 状态类型
     * @param msg 返回内容
     * @param data 数据对象
     */
    public    AjaxResult(Type type, String msg, Object data)
    {
        this.code=type.value();
        this.msg=msg;
        this.data=data;
    }

    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public  AjaxResult success()
    {
        this.code=Type.SUCCESS.value();
        this.msg="操作成功";
        return this;
    }
    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public  AjaxResult success(Object data)
    {
        this.code=Type.SUCCESS.value();
        this.msg="操作成功";
        this.data=data;
        return this;
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @return 成功消息
     */
    public  AjaxResult success(String msg)
    {
        this.code=Type.SUCCESS.value();
        this.msg="操作成功";
        return this;
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public  AjaxResult success(String msg, Object data)
    {
        this.code=Type.SUCCESS.value();
        this.msg=msg;
        this.data=data;
        return this;
    }
    public  AjaxResult warn()
    {
        this.code=Type.WARN.value();
        this.msg="操作失败";
        return this;
    }

    /**
     * 返回警告消息
     * 
     * @param msg 返回内容
     * @return 警告消息
     */
    public  AjaxResult warn(String msg)
    {
        this.code=Type.WARN.value();
        this.msg=msg;
        return this;
    }

    /**
     * 返回警告消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public  AjaxResult warn(String msg, Object data)
    {
        this.code=Type.WARN.value();
        this.msg=msg;
        this.data=data;
        return this;
    }

    /**
     * 返回错误消息
     * 
     * @return
     */
    public  AjaxResult error()
    {
        this.code=Type.ERROR.value();
        this.msg="系统异常";
        return this;
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @return 警告消息
     */
    public  AjaxResult error(String msg)
    {
        this.code=Type.ERROR.value();
        this.msg=msg;
        return this;
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public  AjaxResult error(String msg, Object data)
    {
        this.code=Type.ERROR.value();
        this.msg=msg;
        this.data=data;
        return this;
    }

    /**
     * 没有权限或者权限不足 或者没有登录
     * @return
     */
    public  AjaxResult noPermission()
    {
        this.code=Type.NO_PERMISSION.value();
        this.msg="权限不足";
        return this;
    }

    /**
     * 没有可访问的资源
     * @return
     */
    public  AjaxResult noResource()
    {
        this.code=Type.NO_RESOURCE.value();
        this.msg="没有可访问的资源";
        return this;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
